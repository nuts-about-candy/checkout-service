package com.nutsaboutcandy.cashier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nutsaboutcandy.cashier.entity.model.CheckoutDetails;
import com.nutsaboutcandy.cashier.entity.model.ReportMonth;
import com.nutsaboutcandy.cashier.entity.model.SalesReport;
import com.nutsaboutcandy.cashier.service.CashierService;

@RestController
@RequestMapping("/cashier")
public class CashierController {

	@Autowired
	private CashierService cashierService;
	
	@GetMapping("/order-history/{id}")
	public ResponseEntity<Object> getAllPurchasedproducts(@PathVariable("id") long userId) {
		
		return ResponseEntity.ok()
				.body(cashierService.getAllOrdersByUserId(userId));
	}
	
//	@PostMapping("/checkout")
//	public ResponseEntity<Object> checkout(@RequestBody CheckoutDetails checkoutDetails,
//			@RequestHeader("Authorization") String token) {
//		
//		try {
//			cashierService.checkout(checkoutDetails, token);
//		} catch(RuntimeException ex) {
//			String message = "{\"message\" : \""+ ex.getMessage() +"\"}";
//			return ResponseEntity.badRequest().body(message);
//		}
//		return ResponseEntity.ok().build();
//	}
	
	@GetMapping("/sales-report/{yearMonth}")
	public ResponseEntity<SalesReport> getSalesReport(
			@RequestHeader("Authorization") String token,
			@PathVariable("yearMonth") String yearMonth,
			@RequestParam(name = "category", required = false) String category,
			@RequestParam(name = "productName", required = false) String productName,
			@RequestParam(name = "quantity", required = false) String quantity,
			@RequestParam(name = "weight", required = false) String weight,
			@RequestParam(name = "price", required = false) String price) {
		
		SalesReport result = cashierService.getSalesReport(token, yearMonth, 
				category, productName, quantity, weight, price);
		
		return ResponseEntity.ok().body(result);
	}
	
	@GetMapping("/report-months")
	public ResponseEntity<List<ReportMonth>> getReportMonths() {
		
		return ResponseEntity.ok().body(cashierService.getReportMonths());
	}
	
	
}
