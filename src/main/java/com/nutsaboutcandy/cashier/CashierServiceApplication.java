package com.nutsaboutcandy.cashier;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nutsaboutcandy.cashier.entity.model.CheckoutDetails;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

@SpringBootApplication
public class CashierServiceApplication {

	private static final String RPC_QUEUE_NAME = "rpc_queue";
	
	public static void main(String[] args) {
		
//		queueBasic();
//		queueRPC();
		
		SpringApplication.run(CashierServiceApplication.class, args);
	}

	private static void queueRPC() {

		ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
		factory.setHost("nuts-rabbitmq");
		factory.setRequestedHeartbeat(60);
        factory.setAutomaticRecoveryEnabled(true);
        factory.setConnectionTimeout(5000);
        factory.setNetworkRecoveryInterval(5000);
        
        try (Connection connection = factory.newConnection();
                Channel channel = connection.createChannel()) {
        	
               channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);
               channel.queuePurge(RPC_QUEUE_NAME);

               channel.basicQos(1);

               System.out.println(" [x] Awaiting RPC requests");

               Object monitor = new Object();
               DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                   AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                           .Builder()
                           .correlationId(delivery.getProperties().getCorrelationId())
                           .build();

                   String response = "";

                   try {
//                       String message = new String(delivery.getBody(), "UTF-8");
                       CheckoutDetails checkoutDetails = new ObjectMapper()
                    		   .readValue(delivery.getBody(), CheckoutDetails.class);

                       System.out.println(checkoutDetails);
                       response = "REPLY:OK";
                   } catch (RuntimeException e) {
                       System.out.println(" [.] " + e.toString());
                   } finally {
                       channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
                       channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                       // RabbitMq consumer worker thread notifies the RPC server owner thread
                       synchronized (monitor) {
                           monitor.notify();
                       }
                   }
               };

               channel.basicConsume(RPC_QUEUE_NAME, false, deliverCallback, (consumerTag -> { }));
               // Wait and be prepared to consume the message from RPC client.
               while (true) {
                   synchronized (monitor) {
                       try {
                           monitor.wait();
                       } catch (InterruptedException e) {
                           e.printStackTrace();
                       }
                   }
               }
           } catch (IOException e1) {
        	   e1.printStackTrace();
			} catch (TimeoutException e2) {
				e2.printStackTrace();
			}
	}

	private static void queueBasic() {
		String QUEUE_NAME = "test-queue";
		try {
			ConnectionFactory factory = new ConnectionFactory();
	        factory.setHost("localhost");
	        Connection connection = factory.newConnection();
	        Channel channel = connection.createChannel();

	        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
	        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

	        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
	            String message = new String(delivery.getBody(), "UTF-8");
	            System.out.println(" [x] Received '" + message + "'");
	        };
	        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
