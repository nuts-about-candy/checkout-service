package com.nutsaboutcandy.cashier.msgqueue;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nutsaboutcandy.cashier.entity.model.CheckoutDetails;
import com.nutsaboutcandy.cashier.service.CashierService;
import com.rabbitmq.client.Channel;

@Component
public class MsgQueueListener implements ChannelAwareMessageListener {

	@Autowired
	private CashierService cashierService;
	
	@Override
	public void onMessage(Message message, Channel channel) throws Exception {

		CheckoutDetails checkoutDetails = new ObjectMapper()
				.readValue(message.getBody(), CheckoutDetails.class);
		
		System.err.println("PLACEORDER: " + checkoutDetails);
		if(checkoutDetails != null)
			cashierService.placeOrder(checkoutDetails);
		
//		MessageProperties messageProperties = message.getMessageProperties();
//		AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder()
//				.correlationId(messageProperties.getCorrelationId()).build();
//		
//		System.err.println(checkoutDetails);
//
//		channel.basicPublish("", messageProperties.getReplyTo(), replyProps, "OK".getBytes("UTF-8"));
//		channel.basicAck(messageProperties.getDeliveryTag(), false);
	}
}
