package com.nutsaboutcandy.cashier.entity.model;

import java.util.List;

public class SalesReport {

	private double grossSales;
	private double totalDiscount;
	private double netSales;
	private String yearMonth;
	private int totalQuantity;
	private double totalPrice;
	private long totalWeight;

	private List<SalesItem> salesItems;

	public double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public double getGrossSales() {
		return grossSales;
	}

	public void setGrossSales(double grossSales) {
		this.grossSales = grossSales;
	}

	public double getNetSales() {
		return netSales;
	}

	public void setNetSales(double netSales) {
		this.netSales = netSales;
	}

	public List<SalesItem> getSalesItems() {
		return salesItems;
	}

	public void setSalesItems(List<SalesItem> salesItems) {
		this.salesItems = salesItems;
	}

	@Override
	public String toString() {
		return "SalesReport [grossSales=" + grossSales + ", totalDiscount=" + totalDiscount + ", netSales=" + netSales
				+ ", salesItems=" + salesItems + "]";
	}

	public String getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}

	public int getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public long getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(long totalWeight) {
		this.totalWeight = totalWeight;
	}

}
