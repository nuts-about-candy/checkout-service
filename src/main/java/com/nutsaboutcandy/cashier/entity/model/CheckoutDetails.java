package com.nutsaboutcandy.cashier.entity.model;

import java.time.LocalDateTime;
import java.util.List;

public class CheckoutDetails {

	private long id;
	private User user;
	private List<CartItem> cartItems;
	private double deliveryFee;
	private double subTotal;
	private double total;
	private double discount;
	private long totalWeight;
	private int itemCount;
	private LocalDateTime dateTimeCreated;
	
	public List<CartItem> getCartItems() {
		return cartItems;
	}
	public void setCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
	}
	public double getDeliveryFee() {
		return deliveryFee;
	}
	public void setDeliveryFee(double deliveryFee) {
		this.deliveryFee = deliveryFee;
	}
	public double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public long getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(long totalWeight) {
		this.totalWeight = totalWeight;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public int getItemCount() {
		return itemCount;
	}
	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public LocalDateTime getDateTimeCreated() {
		return dateTimeCreated;
	}
	public void setDateTimeCreated(LocalDateTime dateTimeCreated) {
		this.dateTimeCreated = dateTimeCreated;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "CheckoutDetails [id=" + id + ", user=" + user + ", cartItems=" + cartItems + ", deliveryFee="
				+ deliveryFee + ", subTotal=" + subTotal + ", total=" + total + ", discount=" + discount
				+ ", totalWeight=" + totalWeight + ", itemCount=" + itemCount + ", dateTimeCreated=" + dateTimeCreated
				+ "]";
	}
	
}
