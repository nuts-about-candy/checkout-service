package com.nutsaboutcandy.cashier.entity.model;

import java.util.Map;

public class ProductsWeights {

	private Map<Integer, Long> weights;

	public ProductsWeights() {}
	
	public ProductsWeights(Map<Integer, Long> weights) {
		this.weights = weights;
	}
	
	public Map<Integer, Long> getWeights() {
		return weights;
	}

	public void setWeights(Map<Integer, Long> weights) {
		this.weights = weights;
	}

}
