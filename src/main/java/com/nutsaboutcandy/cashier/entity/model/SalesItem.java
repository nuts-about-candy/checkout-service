package com.nutsaboutcandy.cashier.entity.model;

public class SalesItem {

	private String category;
	private String productName;
	private String classification;
	private int quantity;
	private double price;
	private long weight;

	public SalesItem() {
	}

	public SalesItem(String category, String productName, String classification,
			int quantity, long weight, double price) {
		super();
		this.category = category;
		this.productName = productName;
		this.classification = classification;
		this.quantity = quantity;
		this.weight = weight;
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "SalesItem [category=" + category + ", productName=" + productName + ", classification=" + classification
				+ ", quantity=" + quantity + ", price=" + price + "]";
	}

	public long getWeight() {
		return weight;
	}

	public void setWeight(long weight) {
		this.weight = weight;
	}

}
