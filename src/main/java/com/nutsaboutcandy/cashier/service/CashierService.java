package com.nutsaboutcandy.cashier.service;

import java.util.List;

import com.nutsaboutcandy.cashier.entity.model.CheckoutDetails;
import com.nutsaboutcandy.cashier.entity.model.ReportMonth;
import com.nutsaboutcandy.cashier.entity.model.ReportSummCategory;
import com.nutsaboutcandy.cashier.entity.model.SalesReport;

public interface CashierService {

	void checkout(CheckoutDetails checkoutDetails, String token);

	List<CheckoutDetails> getAllOrdersByUserId(long userId);

	List<ReportSummCategory> getSalesSummaryByCategory();

	List<ReportMonth> getReportMonths();


	SalesReport getSalesReport(String token, String yearMonth, 
			String category, String productName, String quantity, String weight, String price);

	void placeOrder(CheckoutDetails checkoutDetails);

}
