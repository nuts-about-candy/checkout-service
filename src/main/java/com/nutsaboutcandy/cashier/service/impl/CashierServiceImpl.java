package com.nutsaboutcandy.cashier.service.impl;

import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nutsaboutcandy.cashier.entity.model.CartItem;
import com.nutsaboutcandy.cashier.entity.model.CheckoutDetails;
import com.nutsaboutcandy.cashier.entity.model.Product;
import com.nutsaboutcandy.cashier.entity.model.ProductsWeights;
import com.nutsaboutcandy.cashier.entity.model.ReportMonth;
import com.nutsaboutcandy.cashier.entity.model.ReportSummCategory;
import com.nutsaboutcandy.cashier.entity.model.SalesItem;
import com.nutsaboutcandy.cashier.entity.model.SalesReport;
import com.nutsaboutcandy.cashier.mapper.CashierMapper;
import com.nutsaboutcandy.cashier.service.CashierService;

@Service
public class CashierServiceImpl implements CashierService{

	private static final CloseableHttpClient httpclient = HttpClients.createDefault();
	private static final DecimalFormat df2 = new DecimalFormat("#.##");
	
	@Autowired
	private CashierMapper cashierMapper;
	
	
	@Override
	public List<CheckoutDetails> getAllOrdersByUserId(long userId) {
		
		List<CheckoutDetails> list = cashierMapper.findOrdersByUserId(userId);
		list.stream().forEach(order -> {
			List<CartItem> items = cashierMapper.findOrderItemsByOrderId(order.getId());
			order.setCartItems(items);
		});
		
		return list;
	}
	
	@Override
	public void checkout(CheckoutDetails checkoutDetails, String token) {
		
		// CHECK IF PRODUCT QUANTITY IS AVAILABLE
		Map<Integer, Long> productWeight = checkoutDetails.getCartItems()
			.stream().collect(
				Collectors.toMap(
						CartItem::getProductId, CartItem::getTotalWeight, (w1, w2) -> w1+w2));
		
		
		HttpPost httppost = new HttpPost("http://localhost/api/inventory/has-quantity");
		httppost.setHeader("Autorization", token);
		httppost.setHeader("Accept", "application/json");
	    httppost.setHeader("Content-type", "application/json");
	    CloseableHttpResponse response = null;
	    
		try{
			
			httppost.setEntity(new StringEntity(new ObjectMapper()
					.writeValueAsString(new ProductsWeights(productWeight))));
			response = httpclient.execute(httppost);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(response != null)
			if(response.getStatusLine().getStatusCode() == 200) {// INSERT TO DB
				
				placeOrder(checkoutDetails);
				deleteCartItems(checkoutDetails.getCartItems(), token);
			}
			else
				throwError(response, checkoutDetails.getCartItems());
		
	}
	
	@Override
	public List<ReportSummCategory> getSalesSummaryByCategory() {
		
		List<ReportSummCategory> result = cashierMapper.findSalesSummaryByCategory();
		final double total = result.stream()
					.mapToDouble(ReportSummCategory::getTotal)
					.sum();
		
		result.stream().forEach(item -> {
			double percent = (item.getTotal() / total) * 100;
			item.setPercent(Double.parseDouble(df2.format(percent)));
		});
		return result;
		
	}


	@Override
	public SalesReport getSalesReport(String token, String yearMonth, 
			String category, String productName, String quantity, String weight, String price) {

		// GET ALL PRODUCTS FROM INVENTORY
		HttpGet httpGet = new HttpGet("http://localhost/api/inventory/products");
		httpGet.setHeader("Autorization", token);
		httpGet.setHeader("Accept", "application/json");
		httpGet.setHeader("Content-type", "application/json");
		
		Product[] products = null;
	    try{
			
	    	CloseableHttpResponse response = httpclient.execute(httpGet);
	    	
	    	if(response.getStatusLine().getStatusCode() == 200) 
	    		products = new ObjectMapper().readValue(
		    			response.getEntity().getContent(), Product[].class);
	    } catch (IOException e) {
			e.printStackTrace();
		}
	    
	    SalesReport salesReport = new SalesReport();
	    if(products != null) {
	    	
	    	List<CheckoutDetails> salesData = cashierMapper.findPurchasedOrderByMonth(yearMonth);
	    	
	    	double grossSales = salesData.stream()
	    			.mapToDouble(item -> item.getSubTotal()).sum();
	    	double totalDiscount = salesData.stream()
	    			.mapToDouble(item -> item.getDiscount()).sum();
	    	
	    	List<SalesItem> salesItems = Arrays.stream(products)
	    		.map(prod -> getSalesItems(prod, yearMonth))
	    			.collect(Collectors.toList());
	    	
	    	double totalPrice = salesItems.stream()
	    			.mapToDouble(item -> item.getPrice()).sum();
	    	int totalQuantity = salesItems.stream()
	    			.mapToInt(item -> item.getQuantity()).sum();
	    	long totalWeight = salesItems.stream()
	    			.mapToLong(item -> item.getWeight()).sum();
	    	
			salesReport.setTotalPrice(totalPrice);
	    	salesReport.setTotalQuantity(totalQuantity);
	    	salesReport.setTotalWeight(totalWeight);
	    	
	    	double netSales = grossSales - totalDiscount;
			salesReport.setGrossSales(grossSales);
	    	salesReport.setTotalDiscount(totalDiscount);
	    	salesReport.setNetSales(netSales);
	    	salesReport.setSalesItems(salesItems);
	    	salesReport.setYearMonth(yearMonth);
	    	
	    	if(category != null && category.length() > 0)
	    		sortSalesItemByCategory(salesItems, category);
	    	if(productName != null && productName.length() > 0)
	    		sortSalesItemByProductName(salesItems, productName);
	    	if(quantity != null && quantity.length() > 0)
	    		sortSalesItemByQuantity(salesItems, quantity);
	    	if(weight != null && weight.length() > 0)
	    		sortSalesItemByWeight(salesItems, weight);
	    	if(price != null && price.length() > 0)
	    		sortSalesItemByPrice(salesItems, price);
	    }
			    
		return salesReport;
	}

	@Override
	public List<ReportMonth> getReportMonths() {
	
		return cashierMapper.getReportMonths();
	}

	private void sortSalesItemByPrice(List<SalesItem> salesItems, String price) {

		if(price.equals("desc")) 
			salesItems.sort(Comparator.comparing(SalesItem::getPrice).reversed());
		else
			salesItems.sort(Comparator.comparing(SalesItem::getPrice));
	}

	private void sortSalesItemByWeight(List<SalesItem> salesItems, String weight) {

		if(weight.equals("desc")) 
			salesItems.sort(Comparator.comparing(SalesItem::getWeight).reversed());
		else
			salesItems.sort(Comparator.comparing(SalesItem::getWeight));
	}

	private void sortSalesItemByQuantity(List<SalesItem> salesItems, String quantity) {

		if(quantity.equals("desc")) 
			salesItems.sort(Comparator.comparing(SalesItem::getQuantity).reversed());
		else
			salesItems.sort(Comparator.comparing(SalesItem::getQuantity));
	}

	private void sortSalesItemByProductName(List<SalesItem> salesItems, String productName) {

		if(productName.equals("desc")) 
			salesItems.sort(Comparator.comparing(SalesItem::getProductName).reversed());
		else
			salesItems.sort(Comparator.comparing(SalesItem::getProductName));
	}

	private void sortSalesItemByCategory(List<SalesItem> salesItems, String category) {
		
		if(category.equals("desc")) 
			salesItems.sort(Comparator.comparing(SalesItem::getCategory).reversed());
	}

	private SalesItem getSalesItems(Product prod, String yearMonth) {
		
		List<CartItem> items = cashierMapper
				.findOrderItemsByProductIdInMonth(yearMonth, prod.getId());
		
		int quantity = 0;
		long weight = 0;
		double price = 0.0;
		if( !items.isEmpty() ) {
			
			quantity = items.stream()
					.mapToInt(item -> item.getQuantity()).sum();
			weight = items.stream()
					.mapToLong(item -> item.getTotalWeight()).sum();
			price = items.stream()
					.mapToDouble(item -> item.getSubTotal()).sum();
		}
		return new SalesItem(prod.getCategory(), prod.getName(), prod.getClassification(),
				quantity, weight, price);
	}

	private void deleteCartItems(List<CartItem> cartItems, String token) {
		
		cartItems.stream()
			.forEach(item -> {
				try{
					HttpDelete httpDelete = new HttpDelete("http://localhost/api/cart/" + item.getId());
					httpDelete.setHeader("Autorization", token);
					httpclient.execute(httpDelete);
				} catch (IOException e) {
					e.printStackTrace();
				}
			});	
	}

	public void placeOrder(CheckoutDetails checkoutDetails) {
		
		checkoutDetails.setDateTimeCreated(LocalDateTime.now());
		final long orderId = cashierMapper.insertOrder(checkoutDetails);
		checkoutDetails.getCartItems().stream()
			.forEach(item -> {
				item.setOrderId(orderId);
				cashierMapper.insertOrderItem(item);
			});
	}

	private void throwError(CloseableHttpResponse response, List<CartItem> list) {
		
		try {
			
			HttpEntity entity = response.getEntity();
			String errorId = EntityUtils.toString(entity, "UTF-8");
			Optional<CartItem> errorItem = list.stream()
					.filter(item -> item.getProductId() == Integer.parseInt(errorId))
					.findFirst();
			
			throw new RuntimeException(
				String.format("Error! Selected Quantity for product '%s' exceeds current stock.", 
					errorItem.get().getProductDetails().getName()));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
