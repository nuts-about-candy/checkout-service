package com.nutsaboutcandy.cashier.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.nutsaboutcandy.cashier.entity.model.CartItem;
import com.nutsaboutcandy.cashier.entity.model.CheckoutDetails;
import com.nutsaboutcandy.cashier.entity.model.ReportMonth;
import com.nutsaboutcandy.cashier.entity.model.ReportSummCategory;

@Mapper
public interface CashierMapper {

	
	@Select("INSERT INTO PURCHASED_ORDER (user_id, user_first_name, user_last_name, user_access,"
			+ "user_username, delivery_fee, discount, item_count, total_weight, sub_total, total, date_time_created) "
			+ "VALUES "
			+ "(#{user.id}, #{user.firstName}, #{user.lastName}, #{user.access}, #{user.username},"
			+ "#{deliveryFee}, #{discount}, #{itemCount}, #{totalWeight}, #{subTotal}, #{total}, "
			+ "#{dateTimeCreated}) RETURNING id")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty ="id")
	long insertOrder(CheckoutDetails checkoutDetails);

	
	@Select("INSERT INTO ORDER_ITEMS (user_id, order_id, product_id, classification_id, category_id, "
			+ "product_name, classification, category, "
			+ "size_label, prize, weight, "
			+ "quantity, size, total_weight, sub_total) VALUES "
			+ "(#{userId}, #{orderId}, #{productId}, #{productDetails.classificationId}, #{productDetails.categoryId}, "
			+ "#{productDetails.name}, #{productDetails.classification}, #{productDetails.category}, "
			+ "#{productSize.label}, #{productSize.prize}, #{productSize.weight},"
			+ "#{quantity}, #{size}, #{totalWeight}, #{subTotal}) ")
	void insertOrderItem(CartItem item);


	@Select("SELECT * FROM PURCHASED_ORDER WHERE user_id = #{userId} "
			+ "ORDER BY date_time_created DESC")
	@Results({
		@Result(column = "id", property = "id"),
		@Result(column = "user_id", property = "user.id"),
		@Result(column = "delivery_fee", property = "deliveryFee"),
		@Result(column = "discount", property = "discount"),
		@Result(column = "item_count", property = "itemCount"),
		@Result(column = "total_weight", property = "totalWeight"),
		@Result(column = "sub_total", property = "subTotal"),
		@Result(column = "total", property = "total"),
		@Result(column = "date_time_created", property = "dateTimeCreated")
	})
	List<CheckoutDetails> findOrdersByUserId(long userId);

	@Select("SELECT * FROM purchased_order " + 
			"WHERE TO_CHAR(date_time_created::TIMESTAMP, 'yyyy-mm') = #{yearMonth}")
	@Results({
		@Result(column = "id", property = "id"),
		@Result(column = "user_id", property = "user.id"),
		@Result(column = "delivery_fee", property = "deliveryFee"),
		@Result(column = "discount", property = "discount"),
		@Result(column = "item_count", property = "itemCount"),
		@Result(column = "total_weight", property = "totalWeight"),
		@Result(column = "sub_total", property = "subTotal"),
		@Result(column = "total", property = "total"),
		@Result(column = "date_time_created", property = "dateTimeCreated")
	})
	List<CheckoutDetails> findPurchasedOrderByMonth(String yearMonth);
	
	@Select("SELECT oi.quantity, oi.sub_total, oi.total_weight "
			+ "FROM order_items oi "
			+ "JOIN purchased_order po on oi.order_id = po.id "
			+ "WHERE product_id = #{productId} "
			+ "AND TO_CHAR(date_time_created::TIMESTAMP, 'yyyy-mm') = #{yearMonth}")
	@Results({
		@Result(column = "id", property = "id"),
//		@Result(column = "order_id", property = "orderId"),
//		@Result(column = "product_id", property = "productId"),
//		@Result(column = "product_name", property = "productDetails.name"),
//		@Result(column = "classification", property = "productDetails.classification"),
//		@Result(column = "category", property = "productDetails.category"),
//		@Result(column = "classification_id", property = "productDetails.classificationId"),
//		@Result(column = "category_id", property = "productDetails.categoryId"),
//		@Result(column = "size_label", property = "productSize.label"),
//		@Result(column = "prize", property = "productSize.prize"),
//		@Result(column = "weight", property = "productSize.weight"),
		@Result(column = "quantity", property = "quantity"),
//		@Result(column = "size", property = "size"),
		@Result(column = "total_weight", property = "totalWeight"),
		@Result(column = "sub_total", property = "subTotal"),
	})
	List<CartItem> findOrderItemsByProductIdInMonth(String yearMonth, long productId);

	@Select("SELECT * FROM ORDER_ITEMS WHERE order_id = #{orderId}")
	@Results({
		@Result(column = "id", property = "id"),
		@Result(column = "order_id", property = "orderId"),
		@Result(column = "product_id", property = "productId"),
		@Result(column = "product_name", property = "productDetails.name"),
		@Result(column = "classification", property = "productDetails.classification"),
		@Result(column = "category", property = "productDetails.category"),
		@Result(column = "classification_id", property = "productDetails.classificationId"),
		@Result(column = "category_id", property = "productDetails.categoryId"),
		@Result(column = "size_label", property = "productSize.label"),
		@Result(column = "prize", property = "productSize.prize"),
		@Result(column = "weight", property = "productSize.weight"),
		@Result(column = "quantity", property = "quantity"),
		@Result(column = "size", property = "size"),
		@Result(column = "total_weight", property = "totalWeight"),
		@Result(column = "sub_total", property = "subTotal"),
	})
	List<CartItem> findOrderItemsByOrderId(long orderId);


	
	@Select("SELECT category, SUM(sub_total) as total "
			+"FROM public.order_items " 
			+"GROUP BY category")
	@Results({
		@Result(column = "category", property = "category"),
		@Result(column = "total", property = "total")
	})
	List<ReportSummCategory> findSalesSummaryByCategory();


	@Select("SELECT DISTINCT TO_CHAR(date_time_created::TIMESTAMP, 'Mon yyyy') AS label,"
			+ "TO_CHAR(date_time_created::TIMESTAMP, 'yyyy-mm') AS value " + 
			"FROM purchased_order ORDER BY label DESC")
	@Results({
		@Result(column = "label", property = "label"),
		@Result(column = "value", property = "value")
	})
	List<ReportMonth> getReportMonths();


	


	

	
}
