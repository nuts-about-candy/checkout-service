CREATE TABLE IF NOT EXISTS PURCHASED_ORDER (
 id bigserial not null primary key,
 user_id integer not null,
 user_first_name varchar(50) not null,
 user_last_name varchar(50) not null,
 user_access varchar(50) not null,
 user_username varchar(50) not null,
 delivery_fee real,
 discount real,
 item_count integer,
 total_weight integer,
 sub_total real,
 total real,
 date_time_created timestamp not null
);

CREATE TABLE IF NOT EXISTS ORDER_ITEMS (
 id bigserial not null primary key,
 order_id integer not null,
 user_id integer not null,
 product_id integer not null,
 category_id integer not null,
 classification_id integer not null,
 product_name varchar(50) not null,
 classification varchar(10) not null, 
 category varchar(50) not null,
 
 quantity integer not null,
 size varchar(10) not null,
 
 size_label varchar(20) not null,
 prize real not null,
 weight integer not null,
 
 total_weight integer not null,
 sub_total real not null
);